#include "interface.h"
#include "model.h"

using namespace std;

double myabs(cpx value){
    return sqrt(value.real()*value.real()+value.imag()*value.imag());
}

void checkOverlapsWithLast(std::vector<MPS> psi, double warningLevel=1E-3)
{
    for(int i=0; i<psi.size()-1; i++){
        for(int j=psi.size()-1; j<psi.size(); j++){
            if( myabs(innerC(psi[i],psi[j])) >= warningLevel ){
                std::cout << "WARNING: Ovl(" << i << "," << j << ")="
                          << innerC(psi[i],psi[j]) << std::endl;
            }
        }
    }
}

std::vector<MPS> dmrgExcited(const MPS psi0, const MPO &H, const Sweeps &sweeps, int states)
{
    std::vector<MPS> psi;
    std::vector<MPS> wfs;

    for(int i=0; i<states; i++){
        auto [tmp_E, tmp_Psi] = dmrg(H,psi,psi0,sweeps,{"Quiet=",true,"Weight=",6000.0*(double)i});
        psi.push_back(tmp_Psi);
        checkOverlapsWithLast(psi);
        std::string psiNum = "Psi_" + std::to_string(i) + "->";
        ExpCon.calc(tmp_Psi, oMode::b,psiNum,
                    "rtime","mem","dim","E","E0","N","K","Ki",
                    "G2_L/4:L","G2_L/2:L","N1:L","N21:L");
    }
    return psi;
}



int main(int argc, char *argv[])
{
    Experiments("dmrg") = [](){
        auto [sites, psi0, H, sweeps] = prepareExpBasic();
        ExpCon.setSites(sites); ExpCon("E") = H;
        ExpCon.calc(psi0, oMode::b,"mem","dim","E","E0","K","N","N1:L");

        dmrgExcited(psi0,H,sweeps,getI("Nstates"));
    };
    Experiments("dmrgMultipleSweeps") = [](){
        seedRNG(1);
        auto sites = BoseHubbard(getI("L"));
        auto psi = prepareInitState(sites);
        auto H = BHHamiltonian(sites,getI("L"),getI("ms"),getD("g"),getD("P"),getB("PBC"));

        auto sweeps = Sweeps(1);
        sweeps.maxdim() = Args::global().getInt("maxDim");
        sweeps.mindim() = Args::global().getInt("minDim");
        sweeps.cutoff() = 1E-3;
        sweeps.niter() = Args::global().getReal("niter");

        double cut = 1E-3;
        ExpCon.setSites(sites); ExpCon("E") = H;

        int i=1;
        auto [E,psi2] = dmrg(H,psi,sweeps);
        double lastE=E; psi= psi2;

        ExpCon.calc(psi, oMode::b,"Sweep_1->","cutoff= 1.0E-3",
                    "rtime","mem","dim","E","E0","N","K","K0",
                    "G2_L/4:L","G2_L/2:L","N1:L","N21:L");

        for(i=2; i<=1000; i++){
            auto [E,psi2] = dmrg(H,psi,sweeps);
            if(pow((E-lastE)/E,2)<pow(0.05,2)){
                cut /= 10.0;
                sweeps.cutoff() = cut;
            }
            lastE=E;
            psi= psi2;
            char buffer[50];
            sprintf (buffer, "%e", cut);
            std::string sweepNum = "Sweep_" + std::to_string(i) + "->";
            std::string cutoffStr = "cutoff= " + std::string(buffer);
            ExpCon.calc(psi, oMode::b,sweepNum, cutoffStr,
                        "rtime","mem","dim","E","E0","N","K","K0",
                        "G2_L/4:L","G2_L/2:L","N1:L","N21:L");
        }

    };
    Experiments("dmrgWithPotential") = [](){
        seedRNG(1);
        int L = getI("L");

        std::vector<double> V;
        for(double i=-(double)(L-1)/2.0; i<(double)L/2.0; i+=1.0){
            V.push_back(-i*i*getD("V"));
        }

        for(uint i=0; i<V.size(); i++){
            std::cout << V[i] << " | ";
        }
        std::cout << std::endl;

        auto sites = BoseHubbard(getI("L"));
        auto psi0 = prepareInitState(sites);
        auto H = BHHamiltonian(sites,getI("L"),getI("ms"),getD("g"),getD("P"),getB("PBC"),V);
        auto sweeps = prepareSweepClass();

        ExpCon.setSites(sites); ExpCon("E") = H;
        ExpCon.calc(psi0, oMode::b,"mem","dim","E","E0","N","N1:L","K");

        dmrgExcited(psi0,H,sweeps,getI("Nstates"));
    };

    Params.add("L","int","100");
    Params.add("Nstates","int","100");
    Params.add("PBC","bool","1");
    Params.add("g","double","1.0");
    Params.add("P","double","0.0");
    Params.add("ms","int","0");
    Params.add("V","double","0.0");
    Params.add("N","int","-1");
    Params.add("d","int","6");
    Params.add("exp","string","dmrgMultipleSweeps");

    Params.add("Silent","bool","1");
    Params.add("cutoff","double","1E-8");
    Params.add("sweeps","int","4");
    Params.add("minDim","int","1");
    Params.add("maxDim","int","1600");
    Params.add("niter","int","10");
    Params.add("ConserveN","bool","1");
    Params.add("state","string","2-2-2-2-2-L/1*0");

    Params.add("PBSenable","bool","0");
    Params.add("PBSjobid","int","0");

    Params.set(argc,argv);
    prepareObservables();
    Experiments.run();


    return 0;
}
