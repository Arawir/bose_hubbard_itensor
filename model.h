#ifndef MODEL
#define MODEL

#include "itensor/all.h"
#include "bose_hubbard.h"

using namespace itensor;

Sweeps prepareSweepClass()
{
    auto sweeps = Sweeps(Args::global().getInt("sweeps"));
    sweeps.maxdim() = Args::global().getInt("maxDim");
    sweeps.mindim() = Args::global().getInt("minDim");
    sweeps.cutoff() = Args::global().getReal("cutoff");
    sweeps.niter() = Args::global().getReal("niter");
    return sweeps;
}

MPO BHHamiltonian(BoseHubbard &sites,
                  int L,int ms, double g,double P,bool PBC, std::vector<double> V={})
{
    double M = (double)L;
    P = -getD("P")*M_PI;


    double U = g*M;
    double MUp =2.0*M*M+P*P;

    auto ampo = AutoMPO(sites);
    for(int j=1; j<L; j++){
        ampo += -(M*M+P*M*im),"b",j,"bT",j+1;
        ampo += -(M*M-P*M*im),"bT",j,"b",j+1;
    }

    if(PBC){
        ampo += -exp(+im*P)*(M*M+P*M*im),"b",L,"bT",1;
        ampo += -exp(-im*P)*(M*M-P*M*im),"bT",L,"b",1;
    }

    for(int j=1; j<=L; j++){
        ampo += U/2.0,"n(n-1)",j;
        ampo += MUp,"n",j;
    }

    if(V.size()!=0){
        for(int j=1; j<=L; j++){
            ampo += V[j-1],"n",j;
        }
    }

    return toMPO(ampo);
}

std::tuple<BoseHubbard,MPS,MPO,Sweeps> prepareExpBasic()
{
    seedRNG(1);
    auto sites = BoseHubbard(getI("L"));
    auto psi = prepareInitState(sites);
    auto H = BHHamiltonian(sites,getI("L"),getI("ms"),getD("g"),getD("P"),getB("PBC"));
    auto sweeps = prepareSweepClass();

    return std::make_tuple( sites,psi,H,sweeps );
}

std::function<std::vector<std::string>()> generateInitState = [](){
    if(getI("N")>0){
        std::vector<std::string> states;
        int N = getI("N");
        int L = getI("L");
        double a = (double)N/(double)L;
        for(int i=0; i<L; i++){
            if( (double)N/(double)(L-i)>=a ){
                states.push_back("1");
                N--;
            } else {
                if((N==0)&&(i%2==0)){
                    states.insert(states.begin(),"0");
                } else {
                    states.push_back("0");
                }
            }
        }
        return states;
    }
    return parseInitState(Args::global().getString("state"));
};

void prepareObservables()
{
    ExpCon("N") = [](const BoseHubbard &sites){
        auto ampo = AutoMPO(sites);
        for(int i=1; i<=sites.length(); i++){
            ampo += 1,"n",i;
        }
        return toMPO(ampo);
    };

    ExpCon("E0") = [](const BoseHubbard &sites){
        auto ampo = AutoMPO(sites);

        int L = getI("L");
        double g = getD("g");
        double J = (double)pow(L,2);
        double Mu = -2.0*J;
        double U = g*(double)L;
        bool PBC = getB("PBC");

        for(int j=1; j<L; j++){
            ampo += -J,"bT",j,"b",j+1;
            ampo += -J,"b",j,"bT",j+1;
        }

        if(PBC){
            ampo += -J,"bT",L,"b",1;
            ampo += -J,"b",L,"bT",1;
        }

        for(int j=1; j<=L; j++){
            ampo += U/2.0,"n(n-1)",j;
            ampo += -Mu,"n",j;
        }
        return toMPO(ampo);
    };

    ExpCon("K") = [](const BoseHubbard &sites){
        auto ampo = AutoMPO(sites);
        int L = sites.length();
        cpx tmp = im*(double)getI("L")/4.0/M_PI;
        double P = -getD("P");

        for(int j=1; j<L; j++){
            ampo += -tmp,"b",j,"bT",j+1;
            ampo += tmp,"bT",j,"b",j+1;
        }
        if(getB("PBC")){
            ampo += -exp(+im*M_PI*P)*tmp,"b",L,"bT",1;
            ampo += exp(-im*M_PI*P)*tmp,"bT",L,"b",1;
        }
        return toMPO(ampo);
    };
    ExpCon("Ki") = [](const BoseHubbard &sites){
        auto ampo = AutoMPO(sites);
        int L = sites.length();
        cpx tmp = im*(double)getI("L")/4.0/M_PI;
        double P = -getD("P");

        for(int j=1; j<L; j++){
            ampo += -tmp,"b",j,"bT",j+1;
            ampo += tmp,"bT",j,"b",j+1;
        }
        return toMPO(ampo);
    };

    ExpCon("K0") = [](const BoseHubbard &sites){
        auto ampo = AutoMPO(sites);
        int L = sites.length();
        cpx tmp = im*(double)getI("L")/4.0/M_PI;

        for(int j=1; j<L; j++){
            ampo += -tmp,"b",j,"bT",j+1;
            ampo += tmp,"bT",j,"b",j+1;
        }
        if(getB("PBC")){
            ampo += -tmp,"b",L,"bT",1;
            ampo += tmp,"bT",L,"b",1;
        }
        return toMPO(ampo);
    };


    ExpCon("G1r_L/4:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto n_0 = AutoMPO(sites);
            auto n_i = AutoMPO(sites);
            n_0 += 1,"bT",(L+1)/4;
            n_i += 1,"b",i;


            out.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
        }

        return out;
    };

    ExpCon("G1i_L/4:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto n_0 = AutoMPO(sites);
            auto n_i = AutoMPO(sites);
            n_0 += -im,"bT",(L+1)/4;
            n_i += -im,"b",i;
            out.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
        }

        return out;
    };
    ExpCon("G1r_L/2:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto n_0 = AutoMPO(sites);
            auto n_i = AutoMPO(sites);
            n_0 += 1,"bT",(L+1)/2;
            n_i += 1,"b",i;


            out.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
        }

        return out;
    };

    ExpCon("G1i_L/2:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto n_0 = AutoMPO(sites);
            auto n_i = AutoMPO(sites);
            n_0 += -im,"bT",(L+1)/2;
            n_i += -im,"b",i;
            out.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
        }

        return out;
    };


    ExpCon("G2_L/4:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto n_0 = AutoMPO(sites);
            auto n_i = AutoMPO(sites);
            n_0 += 1,"n",(L+1)/4;
            if(i==(L+1)/4){
                n_i += 1,"n-1",i;
            } else {
                n_i += 1,"n",i;
            }

            out.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
        }

        return out;
    };
    ExpCon("G2_L/2:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto n_0 = AutoMPO(sites);
            auto n_i = AutoMPO(sites);
            n_0 += 1,"n",(L+1)/2;
            if(i==(L+1)/2){
                n_i += 1,"n-1",i;
            } else {
                n_i += 1,"n",i;
            }

            out.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
        }

        return out;
    };

    ExpCon("N1:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto ampo = AutoMPO(sites);
            ampo += 1,"n",i;
            out.push_back( toMPO(ampo) );
        }

        return out;
    };
    ExpCon("N21:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto ampo = AutoMPO(sites);
            ampo += 1,"n2",i;
            out.push_back( toMPO(ampo) );
        }

        return out;
    };
}


#endif // MODEL

