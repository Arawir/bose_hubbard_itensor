#ifndef __ITENSOR_KONDO_HEISENBERG_H
#define __ITENSOR_KONDO_HEISENBERG_H
#include "itensor/mps/siteset.h"

namespace itensor {

    class BoseHubbardSite;
    using BoseHubbard = BasicSiteSet<BoseHubbardSite>;

    class BoseHubbardSite
    {
    private:
        Index s;
        int d;

    public:
        BoseHubbardSite() { }
        BoseHubbardSite(Index I) : s(I) { }

        BoseHubbardSite(Args const& args = Args::global())
        {
            auto ts = TagSet("Site,BH");
            if( args.defined("SiteNumber") )
                ts.addTags("n="+str(args.getInt("SiteNumber")));

            auto conserveN = args.getBool("ConserveN",false);
            d = args.getInt("d",3);


            if(conserveN){
                auto qints = Index::qnstorage(d);
                for(int n : range(d)) {
                    qints[n] = QNInt(QN({"N=",n}),1);
                }
                s = Index(std::move(qints),ts);

//                 if(d==2){
//                     s = Index{ QN( {"N=",0} ),1,
//                                QN( {"N=",1} ),1,
//                                Out, ts};
//                 } else if(d==3){
//                     s = Index{ QN( {"N=",0} ),1,
//                                QN( {"N=",1} ),1,
//                                QN( {"N=",2} ),1,
//                                Out, ts};
//                 } else if(d==4){
//                     s = Index{ QN( {"N=",0} ),1,
//                                QN( {"N=",1} ),1,
//                                QN( {"N=",2} ),1,
//                                QN( {"N=",3} ),1,
//                                Out, ts};
//                 } else if(d==5){
//                     s = Index{ QN( {"N=",0} ),1,
//                                QN( {"N=",1} ),1,
//                                QN( {"N=",2} ),1,
//                                QN( {"N=",3} ),1,
//                                QN( {"N=",4} ),1,
//                                Out, ts};
//                 } else if(d==6){
//                     s = Index{ QN( {"N=",0} ),1,
//                                QN( {"N=",1} ),1,
//                                QN( {"N=",2} ),1,
//                                QN( {"N=",3} ),1,
//                                QN( {"N=",4} ),1,
//                                QN( {"N=",5} ),1,
//                                Out, ts};
//                 } else if(d==7){
//                     s = Index{ QN( {"N=",0} ),1,
//                                QN( {"N=",1} ),1,
//                                QN( {"N=",2} ),1,
//                                QN( {"N=",3} ),1,
//                                QN( {"N=",4} ),1,
//                                QN( {"N=",5} ),1,
//                                QN( {"N=",6} ),1,
//                                Out, ts};
//                 } else if(d==8){
//                     s = Index{ QN( {"N=",0} ),1,
//                             QN( {"N=",1} ),1,
//                             QN( {"N=",2} ),1,
//                             QN( {"N=",3} ),1,
//                             QN( {"N=",4} ),1,
//                             QN( {"N=",5} ),1,
//                             QN( {"N=",6} ),1,
//                             QN( {"N=",7} ),1,
//                             Out, ts};
//                 } else if(d==9){
//                     s = Index{ QN( {"N=",0} ),1,
//                             QN( {"N=",1} ),1,
//                             QN( {"N=",2} ),1,
//                             QN( {"N=",3} ),1,
//                             QN( {"N=",4} ),1,
//                             QN( {"N=",5} ),1,
//                             QN( {"N=",6} ),1,
//                             QN( {"N=",7} ),1,
//                             QN( {"N=",8} ),1,
//                             Out, ts};
//                 } else if(d==10){
//                     s = Index{ QN( {"N=",0} ),1,
//                             QN( {"N=",1} ),1,
//                             QN( {"N=",2} ),1,
//                             QN( {"N=",3} ),1,
//                             QN( {"N=",4} ),1,
//                             QN( {"N=",5} ),1,
//                             QN( {"N=",6} ),1,
//                             QN( {"N=",7} ),1,
//                             QN( {"N=",8} ),1,
//                             QN( {"N=",9} ),1,
//                             Out, ts};
//                 } else if(d==11){
//                     s = Index{ QN( {"N=",0} ),1,
//                             QN( {"N=",1} ),1,
//                             QN( {"N=",2} ),1,
//                             QN( {"N=",3} ),1,
//                             QN( {"N=",4} ),1,
//                             QN( {"N=",5} ),1,
//                             QN( {"N=",6} ),1,
//                             QN( {"N=",7} ),1,
//                             QN( {"N=",8} ),1,
//                             QN( {"N=",9} ),1,
//                             QN( {"N=",10} ),1,
//                             Out, ts};
//                 } else if(d==12){
//                     s = Index{ QN( {"N=",0} ),1,
//                             QN( {"N=",1} ),1,
//                             QN( {"N=",2} ),1,
//                             QN( {"N=",3} ),1,
//                             QN( {"N=",4} ),1,
//                             QN( {"N=",5} ),1,
//                             QN( {"N=",6} ),1,
//                             QN( {"N=",7} ),1,
//                             QN( {"N=",8} ),1,
//                             QN( {"N=",9} ),1,
//                             QN( {"N=",10} ),1,
//                             QN( {"N=",11} ),1,
//                             Out, ts};
//                 } else if(d==13){
//                     s = Index{ QN( {"N=",0} ),1,
//                             QN( {"N=",1} ),1,
//                             QN( {"N=",2} ),1,
//                             QN( {"N=",3} ),1,
//                             QN( {"N=",4} ),1,
//                             QN( {"N=",5} ),1,
//                             QN( {"N=",6} ),1,
//                             QN( {"N=",7} ),1,
//                             QN( {"N=",8} ),1,
//                             QN( {"N=",9} ),1,
//                             QN( {"N=",10} ),1,
//                             QN( {"N=",11} ),1,
//                             QN( {"N=",12} ),1,
//                             Out, ts};
//                 } else if(d==14){
//                     s = Index{ QN( {"N=",0} ),1,
//                             QN( {"N=",1} ),1,
//                             QN( {"N=",2} ),1,
//                             QN( {"N=",3} ),1,
//                             QN( {"N=",4} ),1,
//                             QN( {"N=",5} ),1,
//                             QN( {"N=",6} ),1,
//                             QN( {"N=",7} ),1,
//                             QN( {"N=",8} ),1,
//                             QN( {"N=",9} ),1,
//                             QN( {"N=",10} ),1,
//                             QN( {"N=",11} ),1,
//                             QN( {"N=",12} ),1,
//                             QN( {"N=",13} ),1,
//                             Out, ts};
//                 } else {
//                     std::cerr << "ERROR: wrong d number!" << std::endl;
//                 }

             } else {
                s = Index{ d,ts};
             }
        }

        Index index() const
        {
            return s;
        }

        IndexVal state(std::string const& state)
        {
            for(int n=0; n<d; n++){
                if(state == std::to_string(n)){ return s(n+1); }
            }
//            if( state == "0" ){ return s(1); }
//            else if( state == "1" ){ return s(2); }
//            else if( (state == "2") && (d>2) ){ return s(3); }
//            else if( (state == "3") && (d>3) ){ return s(4); }
//            else if( (state == "4") && (d>4) ){ return s(5); }
//            else if( (state == "5") && (d>5) ){ return s(6); }
//            else if( (state == "6") && (d>6) ){ return s(7); }
//            else if( (state == "7") && (d>7) ){ return s(8); }
//            else if( (state == "8") && (d>8) ){ return s(9); }
//            else if( (state == "9") && (d>9) ){ return s(10); }
//            else if( (state == "10") && (d>10) ){ return s(11); }
//            else if( (state == "11") && (d>11) ){ return s(12); }
//            else if( (state == "12") && (d>12) ){ return s(13); }
//            else if( (state == "13") && (d>13) ){ return s(14); }
//            else if( (state == "14") && (d>14) ){ return s(15); }
//            else if( (state == "15") && (d>15) ){ return s(16); }
//            else if( (state == "16") && (d>16) ){ return s(17); }

            //else {
                Error("State " + state + " not recognized");
            //}

            return IndexVal{};
        }


        ITensor op(std::string const& opname, Args const& args) const
        {
            auto sP = prime(s);
            auto Op = ITensor{ dag(s),sP };

            if(opname == "b"){
                for(int i=1; i<d; i++){
                    Op.set(s(i+1),sP(i),std::sqrt((double)i));
                }
            }
            else if(opname == "b2"){
                for(int i=1; i<d-1; i++){
                    Op.set(s(i+2),sP(i),std::sqrt((double)(i*(i+1))));
                }
            }
            else if(opname == "bT"){
                for(int i=1; i<d; i++){
                    Op.set(s(i),sP(i+1),std::sqrt((double)i));
                }
            }
            else if(opname == "bT2"){
                for(int i=1; i<d-1; i++){
                    Op.set(s(i),sP(i+2),std::sqrt((double)(i*(i+1))));
                }
            }
            else if(opname == "n"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),i-1);
                }
            }
            else if(opname == "n-1"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),i-2);
                }
            }
            else if(opname == "n2"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),(i-1)*(i-1));
                }
            }
            else if(opname == "l"){
                for(int i=1; i<d; i++){
                    Op.set(s(i),sP(i),i);
                }
            }
            else if(opname == "I"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),1);
                }
            }
            else if(opname == "n(n-1)"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),(i-1)*(i-2));
                }
            }

            else {
                Error("Operator \"" + opname + "\" name not recognized");
            }

            return Op;
        }
    };


} //namespace itensor

#endif
